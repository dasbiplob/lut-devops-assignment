# specify a suitable source image

FROM node:alpine

WORKDIR /app

COPY package*.json ./

RUN npm install

# copy the application source code files

COPY . .

EXPOSE 3000

# specify the command which runs the application
CMD ["npm", "start"]
